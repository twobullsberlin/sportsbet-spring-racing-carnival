package com.twobulls.sportsbetspringracingcarnival;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import java.io.File;
import java.net.URI;
import java.util.HashSet;


public class MainActivity extends Activity {
    public static final String ACTION_INSTALLAPP = "installDownloadedApp";
    public static final String ACTION_DOWNLOAD_FAILED = "downloadFailed";
    public static final String ACTION_DOWNLOAD_IN_PROGRESS = "downloadInProgress";
    public final String TAG = getClass().getSimpleName();

    // URL to be initially loaded in Webview
    private String baseUrl = "http://sb.affiliates.s3.amazonaws.com/SportsbetSpringRacingCarnival/index.html";


    private Context mContext;
    private HashSet<String> mBypassedUrls = new HashSet<String>();
    private WebView mBrowser;
    private ProgressDialog mProgressDialog;
    private ProgressBar mWebviewRetryProgressBar;
    private LocalBroadcastManager mBroadcastManager;
    private DownloadService mDownloadService;
    private boolean isCancelled = false;
    private AlertDialog mInstallDialog;
    private static String mFileLocation;
    private Dialog mSplashDialog;
//    private Metadata mMetaData;

    private int mLoadProgress;
    private Boolean mWebviewError;

    private boolean mIsVisible = false;
    private AlertDialog mDialogAwaitingShow = null;

    // Location of Sportsbet apk
    private final static String APK_DIRECTION = "http://i.sbstatic.com.au/android/sportsbet.apk";
    // Location of the apk-file to be downloaded to
    private final static String FILE_LOCATION = "/download/SportsbetTest.apk";
    // URL of the website to be loaded externally in browser
    private final static String BROWSER_URL = "https://m.sportsbet.com.au/";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        if (savedInstanceState == null) {
            // Display splash screen
            mSplashDialog = new Dialog(mContext, R.style.SplashScreen);
            mSplashDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mSplashDialog.setContentView(R.layout.splash);
            mSplashDialog.setCancelable(false);
            mSplashDialog.show();


            initialLoadPartlyCompleted();

        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        // Get local broadcast manager
        mBroadcastManager = LocalBroadcastManager.getInstance(mContext);

        // Setup the WebView
        initUrlBypass(mContext);
        mBrowser = (WebView)this.findViewById(R.id.sb_browser_webview);
        mBrowser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mBrowser.getSettings().setJavaScriptEnabled(true);
        mBrowser.getSettings().setDomStorageEnabled(true); // Needed for the Sportsbet site to work
        mBrowser.setWebViewClient(new SBWebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);
        mBrowser.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mBrowser.loadUrl(baseUrl);


        mWebviewRetryProgressBar = (ProgressBar)this.findViewById(R.id.progressbar_webview_retry);
        mWebviewRetryProgressBar.setVisibility(View.INVISIBLE);

        mWebviewError = false;

        // Setup the Button Click listeners
        View.OnClickListener downloadClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    beginApkDownloadAndInstallProcess();
            }
        };
        View.OnClickListener websiteClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BROWSER_URL));
                    startActivity(browserIntent);
            }
        };

        ImageButton btn_install_top = (ImageButton)this.findViewById(R.id.btn_install_top);
        ImageButton btn_install_bottom = (ImageButton)this.findViewById(R.id.btn_install_bottom);
        btn_install_top.setOnClickListener(downloadClickListener);
        btn_install_bottom.setOnClickListener(websiteClickListener);

        Button btn_webview_retry = (Button)this.findViewById(R.id.btn_webview_retry);
        btn_webview_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebviewError = false;

                mWebviewRetryProgressBar.setVisibility(View.VISIBLE);
                mBrowser.reload();
            }
        });

        // Handle the intent action
        handleAction(getIntent().getAction(), mContext);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsVisible = true;
        if (mDialogAwaitingShow != null) {
            mDialogAwaitingShow.show();
            mDialogAwaitingShow = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsVisible = false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(downloadServiceConn);
        // Ensure unregistered so it doesn't use the old receiver closures
        mBroadcastManager.unregisterReceiver(downloadProgressReceiver);
        mBroadcastManager.unregisterReceiver(downloadStateChangeReceiver);
    }

    @Override
    public void onBackPressed() {
        if(mBrowser != null && mBrowser.canGoBack())
            mBrowser.goBack();
        else
            super.onBackPressed();
    }

    /**
     * Handles the intent action.
     * @param action Intent action to be handled
     */
    private void handleAction(String action, Context context) {
        String storageState = Environment.getExternalStorageState();

        // Intent was launched from the notification to install the app
        if (ACTION_INSTALLAPP.equals(action)) {

            showInstallDialog(context);
        }
        // Intent was launched from the notification when the app failed download
        else if (ACTION_DOWNLOAD_FAILED.equals(action)) {
            showFailedDownloadAlert(context);
        }
        // Intent launched from the notification while the app is being downloaded
        else if (ACTION_DOWNLOAD_IN_PROGRESS.equals(action)) {
            showProgressDialog(context);
        }
        // App was launched
        else {
            // No-op
        }

        Intent intent = new Intent(mContext, DownloadService.class);
        bindService(intent, downloadServiceConn, Context.BIND_AUTO_CREATE);
    }

    /**
     * Show the dialog which displays the download progress
     */
    private void showProgressDialog(Context context) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            if (Build.VERSION.SDK_INT >= 11) {
                mProgressDialog.setProgressNumberFormat(null);
            }
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    isCancelled = true;

                    if (mDownloadService != null) {
                        mDownloadService.cancelDownload();
                        mProgressDialog = null;
                    }
                }
            });
            if (mDownloadService != null) {
                mProgressDialog.setProgress(mDownloadService.getCompletion());
            }

            mProgressDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * Shows an alert for when the download has failed
     */
    private void showFailedDownloadAlert(Context context) {
        dismissProgressDialog();
        dismissNotifications();

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.dialog_title_failed_download);
        dialog.setMessage(R.string.dialog_message_failed_download);
        dialog.setNegativeButton("OK", null);
        dialog.setCancelable(true);

        if (mIsVisible) {
            dialog.show();
        }
        else {
            mDialogAwaitingShow = dialog.create();
        }
    }


    /**
     * Show an alert when the apk is ready to be installed
     */
    private void showInstallDialog(Context context) {
        dismissProgressDialog();

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.dialog_title_download_finished);
        dialog.setMessage(R.string.dialog_message_download_finished);
        dialog.setPositiveButton(R.string.dialog_button_install, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismissNotifications();
                installApk();
                mInstallDialog = null;
            }
        });
        dialog.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mInstallDialog = null;
            }
        });
        dialog.setCancelable(true);

        mInstallDialog = dialog.create();

        if (mIsVisible) {
            mInstallDialog.show();
        }
        else {
            mDialogAwaitingShow = mInstallDialog;
        }
    }

    /**
     * Begins the apk download and installation process
     */
    public void beginApkDownloadAndInstallProcess() {
        // Check security settings
        if (!this.canInstallUnknownApps()) {
            // If not enabled, show alert and redirect user to settings page

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle(R.string.dialog_title_security_setting);
            dialogBuilder.setMessage(R.string.dialog_message_security_setting);
            dialogBuilder.setNegativeButton(R.string.dialog_negative_security_setting, null);
            dialogBuilder.setPositiveButton(R.string.dialog_positive_security_setting, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    redirectToSecuritySettings();
                }
            });

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                dialogBuilder.setMessage(R.string.dialog_message_security_setting_pre_ics);

                dialogBuilder.setNeutralButton(R.string.dialog_positive_app_setting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        redirectToAppSettings();
                    }
                });
            }

            dialogBuilder.show();

            return;
        }

        // If there is no SD card inserted then display error
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            new AlertDialog.Builder(mContext)
                    .setTitle(R.string.dialog_title_external_storage_required)
                    .setMessage(R.string.dialog_message_external_storage_required)
                    .setPositiveButton(R.string.dialog_button_external_storage_required, null)
                    .show();
            return;
        }

        mFileLocation = Environment.getExternalStorageDirectory() + FILE_LOCATION;
        this.kickoffDownload();
    }


    /**
     * Begins the download process by first downloading the metadata which contains the link
     * to the latest apk. Once found, it will then begin the downloadNewApk.
     */
    public void kickoffDownload() {
        showProgressDialog(mContext);

        final int ESTIMATED_SIZE = 2200000; // Size of apk at time of version 1

        downloadNewApk(APK_DIRECTION, ESTIMATED_SIZE, mFileLocation);

    }

    private void initialLoadPartlyCompleted() {
        mLoadProgress++;
        if (mLoadProgress > 0) {
            new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    if (mSplashDialog != null) {
                        mSplashDialog.dismiss();
                        mSplashDialog = null;
                    }
                    return false;
                }
            }).sendEmptyMessageDelayed(1, 5000);
        }
    }
    /**
     * Starts the DownloadService to download the apk
     *
     * @param updateUrl The url to the apk
     * @param fileSize The fileSize of the apk used to provide download progress
     * @param fileLocation The local location to save the apk
     */
    private void downloadNewApk( String updateUrl, int fileSize, String fileLocation ) {
        Intent downloadIntent = new Intent(mContext, DownloadService.class);
        downloadIntent.putExtra(DownloadService.EXTRA_URL, updateUrl);
        downloadIntent.putExtra(DownloadService.EXTRA_SIZE, fileSize);
        downloadIntent.putExtra(DownloadService.EXTRA_APK_PATH, fileLocation);

        startService(downloadIntent);
    }

    /**
     * Installs the apk
     */
    public void installApk() {
        if (mInstallDialog != null) {
            mInstallDialog.dismiss();
            mInstallDialog = null;
        }

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);

        File apkFile = new File(mFileLocation);
        intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Redirects the user to the system Application Settings
     */
    private void redirectToAppSettings() {
        startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
    }

    /**
     * Redirects the user to the system Security Settings
     */
    private void redirectToSecuritySettings() {
        startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
    }

    /**
     * Checks whether permissions for allowing install of non market apps has been enabled
     *
     * @return True if permissions exist for enabling install of non market apps
     */
    @SuppressWarnings("deprecation")
    private Boolean canInstallUnknownApps() {
        boolean unknownSource = false;
        if (Build.VERSION.SDK_INT < 3) {
            unknownSource = Settings.System.getInt(getContentResolver(), Settings.System.INSTALL_NON_MARKET_APPS, 0) == 1;
        }
        else if (Build.VERSION.SDK_INT < 17) {
            unknownSource = Settings.Secure.getInt(getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 0) == 1;
        }
        else {
            unknownSource = Settings.Global.getInt(getContentResolver(), Settings.Global.INSTALL_NON_MARKET_APPS, 0) == 1;
        }
        return unknownSource;
    }

    /**
     * Dismisses download notifications from the System Notification panel
     */
    protected void dismissNotifications() {
        // Dismiss notifications
        NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(DownloadService.NOTIFICATION_ID);
    }


    private String getDeviceId() {
        TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = manager.getDeviceId();

        return deviceId;
    }

    private String getAndroidId() {
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    /**
     * Callback when Download has progressed
     */
    private BroadcastReceiver downloadProgressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProgressDialog != null) {
                mProgressDialog.setProgress(mDownloadService.getCompletion());
            }
        }
    };

    /**
     * Callback when Download has changed state
     */
    private BroadcastReceiver downloadStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mDownloadService != null) {
                String state = mDownloadService.getState();

                if (DownloadService.DOWNLOAD_CANCELLED.equals(state)) {
                    dismissProgressDialog();
                }

                else if (DownloadService.DOWNLOAD_FAILED.equals(state)) {
                    showFailedDownloadAlert(mContext);
                }

                else if (DownloadService.DOWNLOAD_FINISHED.equals(state)) {
                    showInstallDialog(mContext);
                }

                else if (DownloadService.DOWNLOAD_RUNNING.equals(state)) {
                    // No-op
                }

            }
        }
    };



    /**
     * Callbacks for registering and un-registering download progress notifications
     */
    private ServiceConnection downloadServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            DownloadService.DownloadServiceBinder binder = (DownloadService.DownloadServiceBinder)iBinder;
            mDownloadService = binder.getService();

            mBroadcastManager.registerReceiver(downloadProgressReceiver, new IntentFilter(DownloadService.DOWNLOAD_PROGRESSED));
            mBroadcastManager.registerReceiver(downloadStateChangeReceiver, new IntentFilter(DownloadService.DOWNLOAD_STATE_CHANGED));

            // If the progress dialog is up, and after binding finds its not running then close the progress dialog.
            if (mProgressDialog != null && mDownloadService.getState() != DownloadService.DOWNLOAD_RUNNING) {
                dismissProgressDialog();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mDownloadService = null;
            mBroadcastManager.unregisterReceiver(downloadProgressReceiver);
            mBroadcastManager.unregisterReceiver(downloadStateChangeReceiver);
        }
    };



    /**
     * Initialises Urls which should be bypassed by the WebView
     * @param context context used for parsing AppConfig
     */
    private void initUrlBypass(Context context) {
        mBypassedUrls.add(baseUrl);
    }

    private class SBWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading( WebView view, String url ) {

            if(url.startsWith("http://ad.doubleclick.net")){
                // Opens doubleClick urls in external browser
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
                return true;
            }

            // As long as only local files are shown, open all links in Webview
            return false;

        }


        @Override
        public void onPageFinished( WebView view, String url ) {
            if (mWebviewError == false && view.getVisibility() == View.INVISIBLE) {
                view.setVisibility(View.VISIBLE);
            }
            mWebviewRetryProgressBar.setVisibility(View.INVISIBLE);
        }


        @Override
        public void onReceivedError( WebView view, int errorCode, String description, String failingUrl ) {
            mWebviewError = true;
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.INVISIBLE);
            }
        }
    }
}
