package com.twobulls.sportsbetspringracingcarnival;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloadService extends Service {
    public static final String DOWNLOAD_NOT_STARTED = "downloadNotStarted";
    public static final String DOWNLOAD_FINISHED = "downloadFinished";
    public static final String DOWNLOAD_CANCELLED = "downloadCancelled";
    public static final String DOWNLOAD_FAILED = "downloadFailed";
    public static final String DOWNLOAD_RUNNING = "downloadRunning";

    public static final String DOWNLOAD_STATE_CHANGED = "stateChanged";
    public static final String DOWNLOAD_PROGRESSED = "downloadProgressed";

    public static final String EXTRA_URL = "url";
    public static final String EXTRA_SIZE = "size";
    public static final String EXTRA_APK_PATH = "filelocation";

    private static final int READ_TIMEOUT = 15000; // millis

    private String mState;

    private final DownloadServiceBinder mBinder = new DownloadServiceBinder();
    private LocalBroadcastManager broadcastManager;
    private int completion;
    private AsyncTask<String, Integer, Void> mTask;

    public static final int NOTIFICATION_ID = 381308;


    public class DownloadServiceBinder extends Binder {
        public DownloadService getService() { return DownloadService.this; }
    }

    protected void setState(String state) {
        mState = state;
        broadcastManager.sendBroadcast(new Intent(DOWNLOAD_STATE_CHANGED));
    }

    public String getState() {
        return mState;
    }
    public int getCompletion() { return completion; }

    public void cancelDownload() {
        if (mTask != null) {
            mTask.cancel(false);
        }
        else {
            this.stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mState = DOWNLOAD_NOT_STARTED;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        if (extras == null || extras.containsKey(EXTRA_URL) == false || extras.containsKey(EXTRA_SIZE) == false) {
            this.showError(R.string.toast_downloaderror);
            this.stopSelf();
        }

        if (!DOWNLOAD_RUNNING.equals(mState)) {
            broadcastManager = LocalBroadcastManager.getInstance(this);
            String url = extras.getString(EXTRA_URL);
            int fileSize = extras.getInt(EXTRA_SIZE);
            String apkPath = extras.getString(EXTRA_APK_PATH);
            this.downloadUpdate(url, fileSize, apkPath);
        }
        return START_STICKY;
    }


    private void showError( int error ) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }



    // Estimated file size is used for progress bar
    private void downloadUpdate( String url, int estimatedFileSize, String apkPath ) {
        // Create a notification
        final NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_progress_text));

        //Log.d("DownloadService", String.format("url: %s, estimatedFileSize: %d, apkPath: %s", url, estimatedFileSize, apkPath));

        // Start the download
        mTask = new AsyncTask<String, Integer, Void>() {

            private int mFileSize;
            private File mApkFile;
            private String mApkPath;

            public AsyncTask<String, Integer, Void> setFileSizeAndApkPath( int fileSize, String apkPath ) {
                mFileSize = fileSize;
                mApkPath = apkPath;
                return this;
            }

            protected void broadcastProgressed() {
                Intent intent = new Intent(DOWNLOAD_PROGRESSED);
                broadcastManager.sendBroadcast(intent);
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();

                if (mApkFile != null) {
                    mApkFile.delete();
                }
                mTask = null;

                setState(DOWNLOAD_CANCELLED);

                notificationManager.cancel(NOTIFICATION_ID);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(String... params) {
                HttpURLConnection c = null;
                try {
                    URL url = new URL(params[0]);
                    c = (HttpURLConnection) url.openConnection();
                    c.setRequestMethod("GET");
                    //c.setDoOutput(false);
                    c.setReadTimeout(READ_TIMEOUT);
                    c.connect();

                    mApkFile = new File(mApkPath);

                    File parentDir = mApkFile.getParentFile();
                    parentDir.mkdirs();
                    FileOutputStream fos = new FileOutputStream(mApkFile);

                    InputStream is = c.getInputStream();

                    setState(DOWNLOAD_RUNNING);

                    int byteCount = 0;
                    byte[] buffer = new byte[1024];
                    int length = 0;
                    while ((length = is.read(buffer)) != -1) {
                        if (isCancelled()) {
                            mApkFile.delete();
                            return null;
                        }
                        fos.write(buffer, 0, length);
                        byteCount += length;
                        publishProgress(byteCount);
                    }
                    fos.close();
                    is.close();

                }
//                catch (IOException e) {
//
//                }
                catch (Exception e) {
                    e.printStackTrace();
                    setState(DOWNLOAD_FAILED);
                    if (mApkFile != null) {
                        mApkFile.delete();
                    }                }
                finally {
                    if (c != null)
                        c.disconnect();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                completion = (int)(((double)values[0]) / ((double)mFileSize) * 100);
                builder.setProgress(100, completion, false);

                builder.setContentIntent(getPendingIntentForActivityAction(MainActivity.ACTION_DOWNLOAD_IN_PROGRESS));
                notificationManager.notify(NOTIFICATION_ID, builder.build());
                broadcastProgressed();
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                if (DOWNLOAD_FAILED.equals(mState)) {
                    builder.setContentIntent(getPendingIntentForActivityAction(MainActivity.ACTION_DOWNLOAD_FAILED));
                    builder.setContentText(getString(R.string.notification_failed_text)).setProgress(0, 0, false);
                    notificationManager.notify(NOTIFICATION_ID, builder.build());
                    broadcastProgressed();
                }
                else {
                    builder.setContentIntent(getPendingIntentForActivityAction(MainActivity.ACTION_INSTALLAPP));
                    builder.setContentText(getString(R.string.notification_completion_text)).setProgress(0, 0, false);
                    notificationManager.notify(NOTIFICATION_ID, builder.build());
                    broadcastProgressed();
                }

                if (mState == DOWNLOAD_RUNNING) {
                    setState(DOWNLOAD_FINISHED);
                }
                mTask = null;

                stopSelf();
            }

            protected PendingIntent getPendingIntentForActivityAction(String action) {
                Intent intent = new Intent(DownloadService.this, MainActivity.class);
                intent.setAction(action);
                return PendingIntent.getActivity(DownloadService.this, 0, intent,PendingIntent.FLAG_CANCEL_CURRENT);
            }

        }.setFileSizeAndApkPath(estimatedFileSize, apkPath).execute(url);
    }

}

